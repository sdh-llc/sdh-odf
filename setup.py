#!/usr/bin/env python
from setuptools import setup, find_packages
import os

setup(
    name='sdh.odf',
    namespace_packages=['sdh'],
    packages=find_packages('src'),
    package_data={'': ['*.*']},
    package_dir={'': 'src'},
    entry_points={},
    eager_resources=['sdh'],
    version='0.0.1',
    license='MIT',
    include_package_data=True,
    zip_safe=False,
    author='Software Development Hub LLC',
    author_email='dev-tools@sdh.com.ua',
    platforms=['OS Independent'],
    install_requires=[
        'Jinja2', 'markdown2', 'Pillow',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries'],
    description='ODF render template',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
    url='https://bitbucket.org/sdh-llc/sdh-odf',
)

