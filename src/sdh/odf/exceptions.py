from xml.parsers.expat import ErrorString

class OdtTemplateException(Exception):
    """ General ODT processing exception"""
    def __init__(self, message, original_exception=None):
        self._message = message
        self._exception = original_exception

    @property
    def exception_class(self):
        return str(type(self))

    @property
    def _exception_msg(self):
        if self._exception:
            return f"{self._exception}"

    def __str__(self):
        return f"ODT exception: [{self.exception_class}]: {self._message} {self._exception_msg}"


class OdtSourceExpatException(OdtTemplateException):
    def __init__(self, xml_source, exception):
        self.xml_source = xml_source.decode('utf-8')
        self._exception = exception

    def _source_lines(self):
        return self.xml_source.splitlines()

    @property
    def _error_str(self):
        return ErrorString(self._exception.code)

    @property
    def dump(self):
        lines = self._source_lines()
        try:
            line = lines[self._exception.lineno-1]
        except IndexError:
            return f"Line {self._exception.lineno} do not found"

        offset_start = max(self._exception.offset-50, 0)
        offset_end = min(self._exception.offset+50, len(line))
        return line[offset_start:self._exception.offset]+"+!+!+!+"+line[self._exception.offset:offset_end]
        
    def __str__(self):
        return f"Template problem {self._error_str} at {self._exception.lineno}:{self._exception.offset}: {self.dump}"